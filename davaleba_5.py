#განსაზღვრეთ x და y ცვლადები, მიანიჭეთ მნიშვნელობა კლავიატურიდან, ცალ-ცალკე
#სტრიქონზე დაბეჭდეთ x-ის y-ზე გაყოფის შედეგად მიღებულ მთელი შედეგი და y-ის x-ზე
#გაყოფის შედეგად მიღებული ნაშთი. (ფორმატის გათვალისწინებით)
x = 9
y = 4
print(str(x)+"/"+str(y)+"="+str(x//y))
print(str(x)+"/"+str(y)+"="+str(x/y))
